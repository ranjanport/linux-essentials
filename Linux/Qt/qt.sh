echo 'Make sure to download the Qt Tar from the same repo where this script is, this contains a custom version modified and has fixed issues of building.'

echo 'Compiled Version of QT is also listed in the same Qt directory use the binary directly or compile it from source.'
sudo mkdir ../Qt
sudo mkdir Qt_core
sudo cd Qt_core

## Enable this line to download if qt is not in the work folder
#sudo wget https://mirrors.cloud.tencent.com/macports/distfiles/qt5/qtbase-everywhere-src-5.15.2.tar.xz
#sudo mv qtbase-everywhere-src-5.15.2.tar.xz qt-everywhere-src-5.15.2.tar.xz

## If downloading from other source Please do the following steps before executing the scripts.
# cd /Qt_core/qt-everywhere-src-5.15.2/qtbase/src/corelib/global
## Add this line to the top of qglobal.h file
# > ifdef __cplusplus
# > #include <limits>
# > endif

sudo tar -xf qt-everywhere-src-5.15.2.tar.xz
sudo cd qt-everywhere-src-5.15.2
#sudo MAKEOPTS="-j6"
sudo ./configure --prefix=../Qt -opensource -nomake examples
sudo make
sudo make install

echo 'Qt is now Installed to ../Qt folder.'

echo 'Issues linked with Builds are represented here'
echo 'https://doc.qt.io/qt-5/linux-requirements.html'
echo 'https://forum.qt.io/topic/136672/error-installing-qt-everywhere-src-5-15-2-on-ubuntu-22-04/3'

