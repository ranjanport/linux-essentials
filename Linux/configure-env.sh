echo 'Getting & Setting Build Libs'
sudo apt-get install -y git
sudo apt-get install -y build-essential 
sudo apt install -y wget
sudo apt install -y cmake
sudo apt-get install -y udev
sudo apt-get install -y flex
sudo apt-get install -y gperf
sudo apt-get install -y bison
sudo apt-get install -y clang
sudo apt install -y cmake-gui
sudo apt-get install -y nodejs
sudo apt remove -y meson
sudo apt install -y meson
echo 'Basic Package Setup Completed'
sudo clear
echo 'Resuming ...'
echo 'Adding Qt Build Dependencies'
sudo apt-get install -y libxi-dev
sudo apt-get install -y libx11-dev
sudo apt-get install -y libxext-dev
sudo apt-get install -y libxcb1-dev
sudo apt-get install -y libxfixes-dev
sudo apt-get install -y libx11-xcb-dev
sudo apt-get install -y libxrender-dev
sudo apt-get install -y libfreetype6-dev
sudo apt-get install -y libxcb-glx0-dev
sudo apt-get install -y libfontconfig1-dev
sudo apt-get install -y libxcb-keysyms1-dev
sudo apt-get install -y libxcb-image0-dev
sudo apt-get install -y libxcb-shm0-dev
sudo apt-get install -y libxcb-icccm4-dev
sudo apt-get install -y libxcb-sync0-dev
sudo apt-get install -y libxcb-xfixes0-dev
sudo apt-get install -y libxcb-shape0-dev
sudo apt-get install -y libxcb-randr0-dev
sudo apt-get install -y libxcb-render-util0-dev
sudo apt-get install -y libxcd-xinerama-dev
sudo apt-get install -y libxkbcommon-dev
sudo apt-get install -y libxkbcommon-x11-dev
sudo apt-get install -y libclang-devel
sudo apt-get install -y libclang-dev
sudo apt-get install -y librust-slog-dev
sudo clear
echo 'Runtime Compile  libraries configured !'
sudo apt-get install -y checkinstall
sudo apt-get install -y libreadline-gplv2-dev 
sudo apt-get install -y libncursesw5-dev
sudo apt-get install -y libssl-dev
sudo apt-get install -y libsqlite3-dev
sudo apt-get install -y tk-dev
sudo apt-get install -y libgdbm-dev
sudo apt-get install -y libc6-dev
sudo apt-get install -y libbz2-dev
sudo apt-get install -y libnss3
sudo apt-get install -y libnss3-dev
sudo apt-get install -y libnss3-dbgsym
sudo apt-get install -y libnss3-tools
sudo apt-get install -y libnss3-tools-dbgsym
sudo apt-get install -y libpango1.0-dev
sudo apt-get install -y ubuntu-snappy-cli
sudo apt-get install -y libgles2-mesa-dev
sudo apt-get install -y libwayland-dev
sudo apt-get install -y wayland-protocols
sudo clear
echo 'Adding Support Packages..'
sudo apt install -y libgl-dev
sudo apt install -y mesa-common-dev
sudo apt install -y libxml2-dev
sudo apt install -y libwxgtk3.0-gtk3-dev
sudo apt install -y libgtk-3-dev
sudo apt install -y libglfw3-dev
sudo apt install -y openjdk-8-jdk
sudo apt install -y patchelf
sudo apt install -y unzip
sudo apt install -y sqlite3



