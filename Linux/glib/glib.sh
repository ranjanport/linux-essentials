echo 'Make sure to download the "glib-*.tar.gz" Tar from the same repo where this script is, this contains a custom version modified and has fixed issues of building.'

sudo mkdir ../Qt
sudo mkdir Qt_core
sudo cd Qt_core

sudo tar xf glib-*.tar.gz
sudo cd glib-*
sudo meson setup _build
sudo meson compile -C _build    
sudo meson install -C _build 

echo 'glib is now fully configured and is globally installed.'
