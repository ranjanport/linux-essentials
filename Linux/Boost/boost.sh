cd /opendrive
echo 'Download the boost tar if you want to re compile the boost or use the compiled version directly (bin)'
#wget http://sourceforge.net/projects/boost/files/boost/1.68.0/boost_1_68_0.tar.gz
tar -xzf boost_1_68_0.tar.gz
mkdir lib
cd boost_1_68_0
./bootstrap.sh
./bjam cxxstd=14 cxxflags=-fPIC cflags=-fPIC --prefix=../lib -a -j8 install
