echo 'Build custom wxWidgets with GTK3 support and no apt dependencies this still includes lot of dependencies like cairo pango etc.'

sudo mkdir ../wxwidget/lib
sudo mkdir wxwidget_core
sudo cd wxwidget_core

## Enable this line to download if qt is not in the work folder
#wget https://github.com/wxWidgets/wxWidgets/releases/download/v3.1.4/wxWidgets-3.1.4.tar.bz2
sudo tar -xf wxWidgets-3.1.4.tar.bz2
sudo cd wxWidgets-3.1.4
sudo mkdir koko
sudo cd koko
sudo ../configure --with-gtk=3 --disable-shared --disable-sys-libs --enable-monolithic --prefix=../wxwidget/lib
sudo make -j8
sudo make install

echo 'WxWidget is now installed in ../wxwidget/lib folder.'
