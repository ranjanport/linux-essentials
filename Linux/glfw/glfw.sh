mkdir glfw*/build/
cmake -S glfw*/ -B glfw*/build 
cd glfw*/
cmake -S . -B build

# To generate VS Solution
#cmake -S path/to/glfw -B path/to/build -G Xcode

cd build/
make
sudo make install



mingw32-runtime
mingw-w64-i686-dev


libglu1-mesa-dev

libilmbase-dev
libfltk1.1-dev libfltk1.3-dev
libcgal-dev

libsage-dev
libsofa1-dev [not armhf]
libvxl1-dev [not armhf]
libroot-core5.34 [armhf]
libroot-core5.34 [i386]
libroot-core5.34 [amd64]
emscripten [not arm64, powerpc, ppc64el]
mingw-w64-common
mingw-w64-x86-64-dev
