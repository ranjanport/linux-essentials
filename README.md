﻿
# Building Scripts for the Listed Libraries:

`NOTE: Please Download the complete repo before proceding further as these Libraries are modified as per success build.`

> Requres Linux 20.04 L.T.S with Python>=2.7.5 configured.
**_List of Libraries_**
 - Qt - 5.15.2
 - Boosts - 1.68.0
 - Glib - 2.77.0
 - WxWidget - 3.1.4

First run the `config-env.sh` file from a non super user terminal. `sh config-env.sh` | If requires a password prompt provide your user password for this.

**Start Building Libraries:**

 - [ ] For Qt run `sh qt.sh` 
 - [ ] For Boost run `sh boost.sh` 
 - [ ] For Glib run `sh glib.sh` 
 - [ ]  For WxWidget run `sh wxwidget.sh`

***NOW AVAILABLE (Static Built of Libraries for Direct Usages)***

 1. Use Pre-Compiled Libraries from each folder instead of building from default
 2.  Consider the Directory Structure first (Root -> opendrive).
++ build [Empty Folder]
++ DatabaseInspector  (Extract the Database Inspector from the official jFrog Repo)
++ lib (Static Lib file from Boost Folder) [Use static or Compile with given instruction]
++ Qt (Static Lib file from Qt Folder) [Use static or Compile with given instruction]
3. Extract the Source file of ***OpenDrive-Converter*** and Rename it to **opendrive**
4. `cd build`
5. `cmake -DCMAKE_INSTALL_PREFIX=.. -DCMAKE_BUILD_TYPE=Release -DBOOST_ROOT=<Complete Path/to/lib/folder> -DBUILD_DBI_PLUGIN=OFF -DBUILD_ODRVIEWER=OFF -G "Unix`
6. `make -j8 install` 

`After Build Sucess the package is written in ../dist folder with the name opendrive-converter-xyz`


